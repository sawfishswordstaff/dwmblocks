//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/	/*Command*/		/*Update Interval*/	/*Update Signal*/
    // this is essentially a margin
    // display ping
	{"^b#ff0000^  Mem: ", "~/.local/bin/mem", 5, 0},
    // display ping
	{"^b#ea51b2^  ", "~/.local/bin/checkping", 20, 0},
    // display volume
	{"^b#626483^  ", "~/.local/bin/getvol", 1, 44},
    // display battery
	{"^b#00f769^^c#000000^  ", "~/.local/bin/battery", 1, 0},
    // display date and time
	{"^d^ 🕒 ", "date '+%b %d %I:%M%P '", 10,	0},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim[] = " ";
static unsigned int delimLen = 5;
